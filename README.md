# Restaurant by strapi

Strapi is the leading open-source headless CMS. It's 100% JavaScript, fully customizable and developer-first. npx create-strapi-app@latest my-project. Get Started. Try the live demo. Top Features. Build apps fast. Building self-hosted, customizable.

Exercise taken from the strapi documentation, for study purposes.

## Getting started

Enter the project folder and type these commands:
```
npm install
npm start 
```
the program to start by npm and open a page in the browser called  [Content-type Builder](localhost:1337/admin/plugins/content-type-builder), after that and register and be able to use the tool.

## 📚 Learn more

- [Resource center](https://strapi.io/resource-center) - Strapi resource center.
- [Strapi documentation](https://docs.strapi.io) - Official Strapi documentation.
- [Strapi tutorials](https://strapi.io/tutorials) - List of tutorials made by the core team and the community.
- [Strapi blog](https://docs.strapi.io) - Official Strapi blog containing articles made by the Strapi team and the community.
- [Changelog](https://strapi.io/changelog) - Find out about the Strapi product updates, new features and general improvements.

Feel free to check out the [Strapi GitHub repository](https://github.com/strapi/strapi). Your feedback and contributions are welcome!

## ✨ Community

- [Discord](https://discord.strapi.io) - Come chat with the Strapi community including the core team.
- [Forum](https://forum.strapi.io/) - Place to discuss, ask questions and find answers, show your Strapi project and get feedback or just talk with other Community members.
- [Awesome Strapi](https://github.com/strapi/awesome-strapi) - A curated list of awesome things related to Strapi.

---

<sub>🤫 Psst! [Strapi is hiring](https://strapi.io/careers).</sub>
